"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var lm = require("lime");
var core_2 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        core_2.Pipe({ name: "safe" }),
        __param(0, core_1.Inject(core_1.forwardRef(function () { return platform_browser_1.DomSanitizer; })))
    ], SafePipe);
    return SafePipe;
}());
exports.SafePipe = SafePipe;
var Mongoosecomponent = /** @class */ (function () {
    function Mongoosecomponent() {
    }
    Mongoosecomponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.logPrefix = "[" + this.widgetContext.getId() + "] ";
        this.lang = this.widgetContext.getLanguage();
        this.ctsid = this.widgetContext.getWidgetInstanceId();
        // Subscribe to the event that is triggered when settings are saved to be able to update the message text
        this.widgetInstance.settingsSaved = function () {
            _this.updateContent();
        };
        // Initial update of the message text and color
        this.updateContent();
    };
    Mongoosecomponent.prototype.updateContent = function () {
        this.configuration = this.widgetContext
            .getSettings()
            .get("Configuration");
        this.messagetype = this.widgetContext.getSettings().get("Message");
        this.form = this.widgetContext.getSettings().get("Form");
        this.formoptions = this.widgetContext
            .getSettings()
            .get("FormOptions");
        this.intialcommand = this.widgetContext
            .getSettings()
            .get("InitialCommand");
        this.addparam = this.widgetContext.getSettings().get("addparam");
        this.refreshIframe();
        //registerHandler
        this.registerHandler();
    };
    Mongoosecomponent.prototype.registerHandler = function () {
        var _this = this;
        var callback = function (args) {
            _this.handleMessage(args);
        };
        infor.companyon.client.registerMessageHandler(this.messagetype, callback);
        lm.Log.debug(this.logPrefix +
            "Message handler registered for message type: " +
            this.messagetype);
    };
    Mongoosecomponent.prototype.handleMessage = function (data) {
        if (data) {
            this.receivedData = data;
            this.receivedDataBoolean = true;
            this.refreshIframe();
        }
        lm.Log.debug(this.logPrefix +
            "Received message from sender widget: " +
            JSON.stringify(data));
    };
    Mongoosecomponent.prototype.refreshIframe = function (url) {
        var mgUrl = "";
        if (!url) {
            mgUrl = this.buildUrl();
        }
        var trustedurl = mgUrl;
        this.mgUrl = trustedurl;
    };
    Mongoosecomponent.prototype.getApplicationurl = function () {
        if (this.widgetContext.getApplication() === undefined ||
            this.widgetContext.getApplication() === null) {
            this.message(lm.WidgetMessageType.Error, this.lang.get("checklogicalid"));
        }
        else {
            var app = this.widgetContext.getApplication();
            var url = (app.useHttps ? "https://" : "http://") +
                app.hostname +
                ":" +
                app.port +
                "/WSWebClient/session/open/";
            return url;
        }
    };
    Mongoosecomponent.prototype.buildUrl = function () {
        var url = this.getApplicationurl();
        var settings = this.widgetContext.getSettings();
        var configuration = this.configuration;
        var option = this.formoptions;
        var form = this.form;
        var init = this.intialcommand;
        var addParm = this.addparam;
        if (configuration) {
            url = url + "" + configuration + "?";
        }
        else {
            url = url + "?";
        }
        if (this.receivedData != undefined) {
            this.receivedData = JSON.stringify(this.receivedData).replace(/,/g, "%2C");
        }
        if (!lm.StringUtil.isNullOrWhitespace(form)) {
            if (!lm.StringUtil.isNullOrWhitespace(option)) {
                if (option === "formonly") {
                    this.receivedDataBoolean === true
                        ? (url =
                            url +
                                "page=formonly&form=" +
                                form +
                                "(SETVARVALUES(VarContextReceived='" +
                                encodeURIComponent(this.receivedData) +
                                "'))&notitle=1&forcesso=1&stype=full&useSendRelay=true")
                        : (url =
                            url +
                                "page=formonly&form=" +
                                form +
                                "(SETVARVALUES(InitialCommand=" +
                                init +
                                "))&notitle=1&forcesso=1&stype=full&useSendRelay=true");
                    this.receivedDataBoolean = false;
                }
                else if (option === "full&useSendRelay=trueoptions") {
                    this.receivedDataBoolean === true
                        ? (url =
                            url +
                                "page=full&useSendRelay=trueoptions&form=" +
                                form +
                                "(SETVARVALUES(VarContextReceived='" +
                                encodeURIComponent(this.receivedData) +
                                "'))&notitle=1&forcesso=1&stype=full&useSendRelay=true")
                        : (url =
                            url +
                                "page=full&useSendRelay=trueoptions&form=" +
                                form +
                                "(SETVARVALUES(InitialCommand=" +
                                init +
                                "))&notitle=1&forcesso=1&stype=full&useSendRelay=true");
                    this.receivedDataBoolean = false;
                }
                else if (option === "toolbaronly") {
                    this.receivedDataBoolean === true
                        ? (url =
                            url +
                                "page=formonly&form=" +
                                form +
                                "(SETVARVALUES(VarContextReceived='" +
                                encodeURIComponent(this.receivedData) +
                                "',DisplayMobileToolbar=top))&notitle=1&forcesso=1&stype=full&useSendRelay=true")
                        : (url =
                            url +
                                "page=formonly&form=" +
                                form +
                                "(SETVARVALUES(InitialCommand=" +
                                init +
                                ",DisplayMobileToolbar=top))&notitle=1&forcesso=1&stype=full&useSendRelay=true");
                    this.receivedDataBoolean = false;
                }
            }
            else {
                this.receivedDataBoolean === true
                    ? (url =
                        url +
                            "page=formonly&form=" +
                            form +
                            "(SETVARVALUES(VarContextReceived='" +
                            encodeURIComponent(this.receivedData) +
                            "'))&notitle=1&forcesso=1&stype=full&useSendRelay=true")
                    : (url =
                        url +
                            "page=formonly&form=" +
                            form +
                            "(SETVARVALUES(InitialCommand=" +
                            init +
                            "))&notitle=1&forcesso=1&stype=full&useSendRelay=true");
                this.receivedDataBoolean = false;
            }
            if (!lm.StringUtil.isNullOrWhitespace(addParm)) {
                url = url + "&" + addParm;
            }
        }
        lm.Log.info(this.logPrefix + " Build Url: " + url);
        return url;
    };
    Mongoosecomponent.prototype.message = function (type, message) {
        this.widgetContext.showWidgetMessage({ type: type, message: message });
    };
    __decorate([
        core_1.Input()
    ], Mongoosecomponent.prototype, "widgetContext");
    __decorate([
        core_1.Input()
    ], Mongoosecomponent.prototype, "widgetInstance");
    Mongoosecomponent = __decorate([
        core_1.Component({
            template: "\n    <div class=\"widget-content busy lm-position-r\" id=\"{{ctsid}}\">\n    <iframe [src]=\"mgUrl | safe\" class=\"lm-fill-absolute\" style=\"border:0\"></iframe>\n    </div>\n\t  "
        })
    ], Mongoosecomponent);
    return Mongoosecomponent;
}());
exports.Mongoosecomponent = Mongoosecomponent;
var MongooseModule = /** @class */ (function () {
    function MongooseModule() {
    }
    MongooseModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [Mongoosecomponent, SafePipe],
            entryComponents: [Mongoosecomponent]
        })
    ], MongooseModule);
    return MongooseModule;
}());
exports.MongooseModule = MongooseModule;
// Widget factory function
exports.widgetFactory = function (context) {
    return {
        angularConfig: {
            moduleType: MongooseModule,
            componentType: Mongoosecomponent
        },
        isConfigured: function (settings) {
            return Boolean(settings.get("Configuration") &&
                settings.get("Form") &&
                settings.get("FormOptions") &&
                settings.get("InitialCommand"));
        }
    };
};
