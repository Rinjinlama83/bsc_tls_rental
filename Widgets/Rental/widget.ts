﻿import {
  AfterViewInit,
  Component,
  Input,
  NgModule,
  Inject,
  forwardRef
} from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  IWidgetComponent,
  IWidgetContext2,
  IWidgetInstance2,
  ILanguage,
  IApplication,
  IWidgetMessage,
  WidgetMessageType,
  IWidgetSettings
} from "lime";
import lm = require("lime");

import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Pipe({ name: "safe" })
export class SafePipe implements PipeTransform {
  constructor(
    @Inject(forwardRef(() => DomSanitizer))
    private sanitizer
  ) {}
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}

@Component({
  template: `
    <div
      *ngIf="mgUrl"
      class="widget-content busy lm-position-r"
      id="{{ ctsid }}"
    >
      <iframe
        [src]="mgUrl | safe"
        class="lm-fill-absolute"
        style="border:0"
      ></iframe>
    </div>
  `
})
export class Mongoosecomponent implements AfterViewInit {
  @Input() widgetContext: IWidgetContext2;
  @Input() widgetInstance: IWidgetInstance2;
  private logPrefix: string;
  private messagetype: string;
  private pageId: string;
  private configuration: string;
  private form: string;
  private formoptions: string;
  private intialcommand: string;
  private logicalId: string;
  private addparam: string;
  private mgUrl: string;
  private ctsid: string;
  private lang: ILanguage;

  constructor() {}

  ngAfterViewInit() {
    this.logPrefix = "[" + this.widgetContext.getId() + "] ";
    this.lang = this.widgetContext.getLanguage();
    this.ctsid = this.widgetContext.getWidgetInstanceId();
    // Subscribe to the event that is triggered when settings are saved to be able to update the message text
    this.widgetInstance.settingsSaved = () => {
      this.updateContent();
    };
    // Initial update of the message text and color
    this.updateContent();
  }

  private updateContent() {
    this.configuration = this.widgetContext
      .getSettings()
      .get<string>("Configuration");
    this.form = this.widgetContext.getSettings().get<string>("Form");
    this.formoptions = this.widgetContext
      .getSettings()
      .get<string>("FormOptions");
    this.intialcommand = this.widgetContext
      .getSettings()
      .get<string>("InitialCommand");
    this.addparam = this.widgetContext.getSettings().get<string>("addparam");
    this.refreshIframe();
  }

  private refreshIframe(url?: string) {
    var mgUrl: string = null;
    if (!url) {
      mgUrl = this.buildUrl();
    }
    let trustedurl = mgUrl;
    this.mgUrl = trustedurl;
  }

  private getApplicationurl() {
    if (
      this.widgetContext.getApplication() === undefined ||
      this.widgetContext.getApplication() === null
    ) {
      this.message(lm.WidgetMessageType.Error, this.lang.get("checklogicalid"));
    } else {
      var app: IApplication = this.widgetContext.getApplication();
      var url =
        (app.useHttps ? "https://" : "http://") +
        app.hostname +
        ":" +
        app.port +
        "/WSWebClient/session/open/";
      return url;
    }
  }

  private buildUrl(): string {
    var url = this.getApplicationurl();
    var settings = this.widgetContext.getSettings();
    var configuration = this.configuration;
    var option = this.formoptions;
    var form = this.form;
    var init = this.intialcommand;
    var addParm = this.addparam;
    if (configuration) {
      url = url + "" + configuration + "?";
    } else {
      url = url + "?";
    }
    if (!lm.StringUtil.isNullOrWhitespace(form)) {
      //url = url + ((!configuration != null && configuration.length > 0) ? "&" : "?");
      if (!lm.StringUtil.isNullOrWhitespace(option)) {
        if (option === "formonly") {
          url =
            url +
            "page=formonly&form=" +
            form +
            "(SETVARVALUES(InitialCommand=" +
            init +
            "))&notitle=1&forcesso=1&stype=full&useSendRelay=true";
        } else if (option === "fulloptions") {
          url =
            url +
            "page=fulloptions&form=" +
            form +
            "(SETVARVALUES(InitialCommand=" +
            init +
            "))&notitle=1&forcesso=1&stype=full&useSendRelay=true";
        } else if (option === "toolbaronly") {
          url =
            url +
            "page=formonly&form=" +
            form +
            "(SETVARVALUES(InitialCommand=" +
            init +
            ",DisplayMobileToolbar=top))&notitle=1&forcesso=1&stype=full&useSendRelay=true";
        }
      } else {
        url =
          url +
          "page=formonly&form=" +
          form +
          "(SETVARVALUES(InitialCommand=" +
          init +
          ",DisplayMobileToolbar=top))&notitle=1&forcesso=1&stype=full&useSendRelay=true";
      }

      if (!lm.StringUtil.isNullOrWhitespace(addParm)) {
        url = url + addParm;
      }
    }
    lm.Log.info(this.logPrefix + " Build Url: " + url);
    return url;
  }
  private message(type: number, message: string): void {
    this.widgetContext.showWidgetMessage({ type: type, message: message });
  }
}

@NgModule({
  imports: [CommonModule],
  declarations: [Mongoosecomponent, SafePipe],
  entryComponents: [Mongoosecomponent]
})
export class MongooseModule {}

// Widget factory function
export var widgetFactory = (context: IWidgetContext2): IWidgetInstance2 => {
  return {
    angularConfig: {
      moduleType: MongooseModule,
      componentType: Mongoosecomponent
    },
    isConfigured: (settings: IWidgetSettings) => {
      return Boolean(
        settings.get<string>("Configuration") &&
          settings.get<string>("Form") &&
          settings.get<string>("FormOptions") &&
          settings.get<string>("InitialCommand")
      );
    }
  };
};
