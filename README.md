# BSC_TLS

Rental Home Page Widgets<br />

### Prerequisites

CSI<br />
Mingle HomePage<br />
Please note - This widgets built only for single tenant and config should be CSI_Demo_Dals<br />

### Forms <br />

BSC_TLS_CustomerLookup <br />
BSC_TLS_CustomerLookup.Tile <br />
BSC_TLS_CustomerOrder <br />
BSC_TLS_GanttChart <br />
BSC_TLS_Map <br />
BSC_TLS_SytelineCalendar <br />
BSC_TLS_Truck <br />
BSC_TLS_TruckLoad <br />

### UET

Fields <br />
Uf_BSC_GDA_Height decimal 18,2 <br />
Uf_BSC_GDA_length decimal 18,2 <br />
Uf_BSC_GDA_width decimal 18,2 <br />
Uf_BSC_GDA_DeliveryDate datetime datetime<br />
Uf_BSC_GDA_TruckModel nvarchar 250<br />

### Class

BSC_GDA <br />
BSC_GDA_SlCoItems <br />

### Class Table

BSC_GDA = > item_mst <br />
BSC_GDA_SlCoItems = > coitem_mst <br />

### Stored Procedure

BSC_BOOM_StroProcs<br />

### Table

BSC_Boom_table<br />

### Widget

Deploy Rental and Rental Message widget on Homepage and import widget.json<br />
