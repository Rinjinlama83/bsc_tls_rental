USE [CSI_Demo_App]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_AvailableBoom]    Script Date: 12/5/2016 4:25:55 PM ******/
DROP PROCEDURE [dbo].[BSC_Boom_AvailableBoom]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_AvailableBoom]    Script Date: 12/5/2016 4:25:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Demo Services
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BSC_Boom_AvailableBoom]
	-- Add the parameters for the stored procedure here
	@WantStartDate Datetime, 
	@WantEndDate Datetime,
	@CoNum CoNumType,
	@Warehouse nvarchar(120)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @BoomID nvarchar(250)
	Declare @temp nvarchar(250)

		(select BoomID,HomeLocation from BSC_Boom_Boom_Assets where BoomID not in 

		-- Sub query four condition for date 
		(select BoomID from BSC_Boom_Commitments
		--range 11-06 amd 11-12
		where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		--complete overlap requested date starts before and end after
		union 
		select BoomID from BSC_Boom_Commitments
		--range 11-6 amd 11-07
		where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		--overlap with start date
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-10 amd 11-11
		where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-08 and 11-09
		where StartDate<=@WantStartDate and EndDate>=@WantEndDate) and HomeLocation=@Warehouse)



	
END


GO

USE [CSI_Demo_App]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_CommitmentsValidate]    Script Date: 12/5/2016 4:26:05 PM ******/
DROP PROCEDURE [dbo].[BSC_Boom_CommitmentsValidate]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_CommitmentsValidate]    Script Date: 12/5/2016 4:26:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BSC_Boom_CommitmentsValidate]
	-- Add the parameters for the stored procedure here
	@WantStartDate Datetime, 
	@WantEndDate Datetime,
	@CoNum CoNumType,
	@WareHouse nvarchar(500),
	@BoominputID nvarchar(500),
	@BoomColor nvarchar(500),
	@SuccessFlag nvarchar(250) output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Declare @BoomID nvarchar(250)
--Declare @temp nvarchar(250)

		--set @BoomID =(select Top(1) BoomID from BSC_Boom_Boom_Assets where BoomID not in 

		---- Sub query four condition for date 
		--(select BoomID from BSC_Boom_Commitments
		----range 11-06 amd 11-12
		--where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		----complete overlap requested date starts before and end after
		--union 
		--select BoomID from BSC_Boom_Commitments
		----range 11-6 amd 11-07
		--where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		----overlap with start date
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-10 amd 11-11
		--where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-08 and 11-09
		--where StartDate<=@WantStartDate and EndDate>=@WantEndDate))


		--set @temp=''	
		--(select @temp=@temp+ BoomID +' , 'from BSC_Boom_Boom_Assets where BoomID not in 

		---- Sub query four condition for date 
		--(select BoomID from BSC_Boom_Commitments
		----range 11-06 amd 11-12
		--where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		----complete overlap requested date starts before and end after
		--union 
		--select BoomID from BSC_Boom_Commitments
		----range 11-6 amd 11-07
		--where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		----overlap with start date
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-10 amd 11-11
		--where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-08 and 11-09
		--where StartDate<=@WantStartDate and EndDate>=@WantEndDate))

		--set @AvailableFlag=(select SUBSTRING(@temp, 0, LEN(@temp)))

		--Check BoomID duplicate or not 
		--if len(@BoomID)>0
		--Begin
		--set @BoomAssign=@BoomID
		--set @SuccessFlag='true'
		----Check @CoNum duplicate or not 

		if  @BoominputID in (select BoomID from BSC_Boom_Boom_Assets where BoomID not in 

		-- Sub query four condition for date 
		(select BoomID from BSC_Boom_Commitments
		--range 11-06 amd 11-12
		where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		--complete overlap requested date starts before and end after
		union 
		select BoomID from BSC_Boom_Commitments
		--range 11-6 amd 11-07
		where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		--overlap with start date
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-10 amd 11-11
		where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-08 and 11-09
		where StartDate<=@WantStartDate and EndDate>=@WantEndDate))

		Begin

			if @CoNum not in (select CustomerOrderID from BSC_Boom_Commitments where BoomID=@BoominputID and @WantStartDate=StartDate and @WantEndDate=EndDate) 
		
			Begin

				if @CoNum not in (select CustomerOrderID from BSC_Boom_Commitments where CustomerOrderID=@CoNum and @WantStartDate=StartDate and @WantEndDate=EndDate) 
					Begin
						Insert into BSC_Boom_Commitments(BoomID,CustomerOrderID,StartDate,EndDate,BoomColor) values
					(@BoominputID,@CoNum,@WantStartDate,@WantEndDate,@BoomColor)
					set @SuccessFlag='Success'
					End

				Else
					Begin
					set @SuccessFlag='DeliverSameOrderRange'
					End

			End 
		
		End 
	


END

	

GO

USE [CSI_Demo_App]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_CommitmentsValidateCO]    Script Date: 12/5/2016 4:26:15 PM ******/
DROP PROCEDURE [dbo].[BSC_Boom_CommitmentsValidateCO]
GO

/****** Object:  StoredProcedure [dbo].[BSC_Boom_CommitmentsValidateCO]    Script Date: 12/5/2016 4:26:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BSC_Boom_CommitmentsValidateCO]
	-- Add the parameters for the stored procedure here
	@WantStartDate Datetime, 
	@WantEndDate Datetime,
	@CoNum CoNumType,
	@WareHouse nvarchar(500),
	@BoominputID nvarchar(500),
	@BoomColor nvarchar(500),
	@ShiptoAddr nvarchar(500),
	@WareHouseAddr nvarchar(500),
	@SuccessFlag nvarchar(250) output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--Declare @BoomID nvarchar(250)
--Declare @temp nvarchar(250)

		--set @BoomID =(select Top(1) BoomID from BSC_Boom_Boom_Assets where BoomID not in 

		---- Sub query four condition for date 
		--(select BoomID from BSC_Boom_Commitments
		----range 11-06 amd 11-12
		--where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		----complete overlap requested date starts before and end after
		--union 
		--select BoomID from BSC_Boom_Commitments
		----range 11-6 amd 11-07
		--where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		----overlap with start date
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-10 amd 11-11
		--where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-08 and 11-09
		--where StartDate<=@WantStartDate and EndDate>=@WantEndDate))


		--set @temp=''	
		--(select @temp=@temp+ BoomID +' , 'from BSC_Boom_Boom_Assets where BoomID not in 

		---- Sub query four condition for date 
		--(select BoomID from BSC_Boom_Commitments
		----range 11-06 amd 11-12
		--where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		----complete overlap requested date starts before and end after
		--union 
		--select BoomID from BSC_Boom_Commitments
		----range 11-6 amd 11-07
		--where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		----overlap with start date
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-10 amd 11-11
		--where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		--union
		--select BoomID from BSC_Boom_Commitments
		----range 11-08 and 11-09
		--where StartDate<=@WantStartDate and EndDate>=@WantEndDate))

		--set @AvailableFlag=(select SUBSTRING(@temp, 0, LEN(@temp)))

		--Check BoomID duplicate or not 
		--if len(@BoomID)>0
		--Begin
		--set @BoomAssign=@BoomID
		--set @SuccessFlag='true'
		----Check @CoNum duplicate or not 

		if  @BoominputID in (select BoomID from BSC_Boom_Boom_Assets where BoomID not in 

		-- Sub query four condition for date 
		(select BoomID from BSC_Boom_Commitments
		--range 11-06 amd 11-12
		where StartDate>=@WantStartDate and EndDate<=@WantEndDate
		--complete overlap requested date starts before and end after
		union 
		select BoomID from BSC_Boom_Commitments
		--range 11-6 amd 11-07
		where StartDate>=@WantStartDate and StartDate<=@WantEndDate
		--overlap with start date
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-10 amd 11-11
		where EndDate>=@WantStartDate and EndDate<=@WantEndDate
		union
		select BoomID from BSC_Boom_Commitments
		--range 11-08 and 11-09
		where StartDate<=@WantStartDate and EndDate>=@WantEndDate))

		Begin

			if @CoNum not in (select CustomerOrderID from BSC_Boom_Commitments where BoomID=@BoominputID and @WantStartDate=StartDate and @WantEndDate=EndDate) 
		
			Begin

				if @CoNum not in (select CustomerOrderID from BSC_Boom_Commitments where CustomerOrderID=@CoNum and @WantStartDate=StartDate and @WantEndDate=EndDate) 
					Begin
						Insert into BSC_Boom_Commitments(BoomID,CustomerOrderID,StartDate,EndDate,BoomColor,ShiptoAddress,WareHouseAddress,WareHouse) values
					(@BoominputID,@CoNum,@WantStartDate,@WantEndDate,@BoomColor,@ShiptoAddr,@WareHouseAddr,@WareHouse)
					set @SuccessFlag='Success'
					End

				Else
					Begin
					set @SuccessFlag='DeliverSameOrderRange'
					End

			End 
		
		End 
	


END

	

GO

