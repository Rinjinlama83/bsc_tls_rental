USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_Boom_Boom_Assets]    Script Date: 7/20/2017 4:56:15 PM ******/
DROP TABLE [dbo].[BSC_Boom_Boom_Assets]
GO

/****** Object:  Table [dbo].[BSC_Boom_Boom_Assets]    Script Date: 7/20/2017 4:56:15 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[BSC_Boom_Boom_Assets](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_Boom_Boom_Assets_InWorkflow]  DEFAULT ((0)),
	[BoomID] [nvarchar](250) NULL,
	[Description] [nvarchar](500) NULL,
	[HomeLocation] [nvarchar](250) NULL,
	[Color] [nvarchar](250) NULL,
	[Image] [varbinary](max) NULL,
 CONSTRAINT [IX_BSC_Boom_Boom_Assets_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO




USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_Boom_Commitments]    Script Date: 7/20/2017 4:56:34 PM ******/
DROP TABLE [dbo].[BSC_Boom_Commitments]
GO

/****** Object:  Table [dbo].[BSC_Boom_Commitments]    Script Date: 7/20/2017 4:56:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_Boom_Commitments](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_Boom_Commitments_InWorkflow]  DEFAULT ((0)),
	[BoomID] [nvarchar](250) NULL,
	[CustomerOrderID] [nvarchar](250) NULL,
	[EndDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[BoomColor] [nvarchar](250) NULL,
	[ShiptoAddress] [nvarchar](250) NULL,
	[TravelTimetoShipAddre] [nvarchar](250) NULL,
	[TravelTimetoWareHouse] [nvarchar](250) NULL,
	[WareHouseAddress] [nvarchar](500) NULL,
	[WareHouse] [nvarchar](250) NULL,
 CONSTRAINT [IX_BSC_Boom_Commitments_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO




USE [CSI_Demo_App]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_InWorkflow]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_NoteExistsFlag]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_RowPointer]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_RecordDate]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_CreateDate]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_UpdatedBy]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] DROP CONSTRAINT [DF_BSC_Boom_Warehouse_CreatedBy]
GO

/****** Object:  Table [dbo].[BSC_Boom_Warehouse]    Script Date: 7/20/2017 4:56:45 PM ******/
DROP TABLE [dbo].[BSC_Boom_Warehouse]
GO

/****** Object:  Table [dbo].[BSC_Boom_Warehouse]    Script Date: 7/20/2017 4:56:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_Boom_Warehouse](
	[CreatedBy] [dbo].[UsernameType] NOT NULL,
	[UpdatedBy] [dbo].[UsernameType] NOT NULL,
	[CreateDate] [dbo].[CurrentDateType] NOT NULL,
	[RecordDate] [dbo].[CurrentDateType] NOT NULL,
	[RowPointer] [dbo].[RowPointerType] NOT NULL,
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL,
	[InWorkflow] [dbo].[FlagNyType] NOT NULL,
	[BOOMID] [nvarchar](250) NULL,
	[Location] [nvarchar](250) NULL,
 CONSTRAINT [IX_BSC_Boom_Warehouse_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_CreatedBy]  DEFAULT (suser_sname()) FOR [CreatedBy]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_UpdatedBy]  DEFAULT (suser_sname()) FOR [UpdatedBy]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_RecordDate]  DEFAULT (getdate()) FOR [RecordDate]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_RowPointer]  DEFAULT (newid()) FOR [RowPointer]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_NoteExistsFlag]  DEFAULT ((0)) FOR [NoteExistsFlag]
GO

ALTER TABLE [dbo].[BSC_Boom_Warehouse] ADD  CONSTRAINT [DF_BSC_Boom_Warehouse_InWorkflow]  DEFAULT ((0)) FOR [InWorkflow]
GO

