USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_Calendar]    Script Date: 12/5/2016 3:45:43 PM ******/
DROP TABLE [dbo].[BSC_GDA_Calendar]
GO

/****** Object:  Table [dbo].[BSC_GDA_Calendar]    Script Date: 12/5/2016 3:45:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_Calendar](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Calendar_InWorkflow]  DEFAULT ((0)),
	[DeliveryDate] [datetime] NOT NULL,
	[WeekDay] [int] NOT NULL,
	[Holiday] [int] NOT NULL,
 CONSTRAINT [IX_BSC_GDA_Calendar_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_ColorCodes]    Script Date: 12/5/2016 3:46:10 PM ******/
DROP TABLE [dbo].[BSC_GDA_ColorCodes]
GO

/****** Object:  Table [dbo].[BSC_GDA_ColorCodes]    Script Date: 12/5/2016 3:46:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_ColorCodes](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_ColorCodes_InWorkflow]  DEFAULT ((0)),
	[ColorCode] [nvarchar](250) NULL,
	[ColorName] [nvarchar](250) NULL,
 CONSTRAINT [IX_BSC_GDA_ColorCodes_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRoutes]    Script Date: 12/5/2016 3:46:25 PM ******/
DROP TABLE [dbo].[BSC_GDA_MilkRoutes]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRoutes]    Script Date: 12/5/2016 3:46:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_MilkRoutes](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRoutes_InWorkflow]  DEFAULT ((0)),
	[Route_ID] [nvarchar](30) NOT NULL,
	[Route_Desc] [nvarchar](100) NULL,
	[Day] [int] NULL,
	[Truck_Type] [nvarchar](20) NULL,
 CONSTRAINT [IX_BSC_GDA_MilkRoutes_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRunCalendar]    Script Date: 12/5/2016 3:46:42 PM ******/
DROP TABLE [dbo].[BSC_GDA_MilkRunCalendar]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRunCalendar]    Script Date: 12/5/2016 3:46:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_MilkRunCalendar](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunCalendar_InWorkflow]  DEFAULT ((0)),
	[Run_Date] [date] NOT NULL,
	[Truck_Model] [nvarchar](50) NULL,
	[Route_ID] [nvarchar](10) NULL,
	[TruckPlates] [nvarchar](50) NULL,
	[Truck_Count] [int] NULL,
 CONSTRAINT [IX_BSC_GDA_MilkRunCalendar_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRunDetail]    Script Date: 12/5/2016 3:46:58 PM ******/
DROP TABLE [dbo].[BSC_GDA_MilkRunDetail]
GO

/****** Object:  Table [dbo].[BSC_GDA_MilkRunDetail]    Script Date: 12/5/2016 3:46:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_MilkRunDetail](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_MilkRunDetail_InWorkflow]  DEFAULT ((0)),
	[MR_RowPointer] [dbo].[RowPointerType] NOT NULL,
	[Co_Num] [dbo].[CoNumType] NOT NULL,
	[Route_ID] [nvarchar](10) NULL,
	[Sequence] [int] NULL,
	[Remarks] [nvarchar](100) NULL,
 CONSTRAINT [IX_BSC_GDA_MilkRunDetail_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_Route]    Script Date: 12/5/2016 3:47:14 PM ******/
DROP TABLE [dbo].[BSC_GDA_Route]
GO

/****** Object:  Table [dbo].[BSC_GDA_Route]    Script Date: 12/5/2016 3:47:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_Route](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Route_InWorkflow]  DEFAULT ((0)),
	[Route_ID] [nvarchar](10) NULL,
	[Route_Desc] [nvarchar](100) NULL,
 CONSTRAINT [IX_BSC_GDA_Route_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_TrackTruck]    Script Date: 12/5/2016 3:47:26 PM ******/
DROP TABLE [dbo].[BSC_GDA_TrackTruck]
GO

/****** Object:  Table [dbo].[BSC_GDA_TrackTruck]    Script Date: 12/5/2016 3:47:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_TrackTruck](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TrackTruck_InWorkflow]  DEFAULT ((0)),
	[model] [nvarchar](250) NULL,
	[volume] [decimal](18, 2) NULL,
	[weight] [decimal](18, 2) NULL,
 CONSTRAINT [IX_BSC_GDA_TrackTruck_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_Truck]    Script Date: 12/5/2016 3:47:41 PM ******/
DROP TABLE [dbo].[BSC_GDA_Truck]
GO

/****** Object:  Table [dbo].[BSC_GDA_Truck]    Script Date: 12/5/2016 3:47:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_Truck](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_Truck_InWorkflow]  DEFAULT ((0)),
	[LicensePlate] [nvarchar](250) NULL,
	[TruckModel] [nvarchar](250) NULL,
	[weight] [decimal](18, 2) NULL,
	[height] [decimal](18, 2) NULL,
	[volume] [decimal](18, 2) NULL,
 CONSTRAINT [IX_BSC_GDA_Truck_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckColorCode]    Script Date: 12/5/2016 3:47:56 PM ******/
DROP TABLE [dbo].[BSC_GDA_TruckColorCode]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckColorCode]    Script Date: 12/5/2016 3:47:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_TruckColorCode](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckColorCode_InWorkflow]  DEFAULT ((0)),
	[ColorName] [nvarchar](250) NULL,
	[Percentage] [decimal](18, 2) NULL,
	[Color] [nvarchar](250) NULL,
 CONSTRAINT [IX_BSC_GDA_TruckColorCode_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckCount]    Script Date: 12/5/2016 3:48:10 PM ******/
DROP TABLE [dbo].[BSC_GDA_TruckCount]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckCount]    Script Date: 12/5/2016 3:48:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_TruckCount](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckCount_InWorkflow]  DEFAULT ((0)),
	[RouteDate] [dbo].[DateType] NOT NULL,
	[RouteID] [nvarchar](10) NOT NULL,
	[TruckCount] [int] NOT NULL,
	[TruckType] [nvarchar](20) NOT NULL,
 CONSTRAINT [IX_BSC_GDA_TruckCount_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckModel]    Script Date: 12/5/2016 3:48:34 PM ******/
DROP TABLE [dbo].[BSC_GDA_TruckModel]
GO

/****** Object:  Table [dbo].[BSC_GDA_TruckModel]    Script Date: 12/5/2016 3:48:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_GDA_TruckModel](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_GDA_TruckModel_InWorkflow]  DEFAULT ((0)),
	[Truck_Model] [nvarchar](50) NOT NULL,
	[Desc] [nvarchar](100) NULL,
	[Volume_Capacity] [decimal](18, 2) NULL,
	[Weight_Capacity] [decimal](18, 2) NULL,
 CONSTRAINT [IX_BSC_GDA_TruckModel_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

