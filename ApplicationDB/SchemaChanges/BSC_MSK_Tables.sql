USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_MSK_Detail]    Script Date: 8/8/2016 9:57:59 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_MSK_Detail](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MSK_Detail_InWorkflow]  DEFAULT ((0)),
	[Items] [dbo].[ItemType] NULL,
	[ParentItems] [dbo].[ItemType] NULL,
	[Qty] [decimal](18, 2) NULL,
	[Description] [nvarchar](512) NULL,
	[OrderNum] [int] NULL CONSTRAINT [DF_BSC_MSK_Detail_OrderNum]  DEFAULT ((40)),
	[OrderLineNum] [int] NULL CONSTRAINT [DF_BSC_MSK_Detail_OrderLineNum]  DEFAULT ((40)),
 CONSTRAINT [IX_BSC_MSK_Detail_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_MSK_SubDetail]    Script Date: 8/8/2016 9:58:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_MSK_SubDetail](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_MSK_SubDetail_InWorkflow]  DEFAULT ((0)),
	[Description] [nvarchar](512) NULL,
	[DueDate] [datetime] NULL,
	[PoLine] [nvarchar](250) NULL,
	[PoNum] [nvarchar](250) NULL,
	[Qty] [int] NULL,
	[State] [nvarchar](250) NULL,
	[Warehouse] [nvarchar](250) NULL,
	[items] [nvarchar](250) NULL,
	[Parentitems] [nvarchar](250) NULL,
 CONSTRAINT [IX_BSC_MSK_SubDetail_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

