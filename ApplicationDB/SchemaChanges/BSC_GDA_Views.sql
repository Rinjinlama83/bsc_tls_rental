USE [CSI_Demo_App]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_CO_Weight'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_CO_Weight'

GO

/****** Object:  View [dbo].[BSC_GDA_CO_Weight]    Script Date: 12/5/2016 3:50:11 PM ******/
DROP VIEW [dbo].[BSC_GDA_CO_Weight]
GO

/****** Object:  View [dbo].[BSC_GDA_CO_Weight]    Script Date: 12/5/2016 3:50:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[BSC_GDA_CO_Weight]
AS
SELECT   coi.co_num, SUM(it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width * coi.qty_ordered) AS LineVolume, SUM(it.Uf_BSC_GDA_weight * coi.qty_ordered) AS LineWeight
FROM     dbo.coitem_mst AS coi LEFT OUTER JOIN
             dbo.item_mst AS it ON it.item = coi.item
GROUP BY coi.co_num

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "coi"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "it"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 404
               Right = 460
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_CO_Weight'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_CO_Weight'
GO

USE [CSI_Demo_App]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_LineWeight'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_LineWeight'

GO

/****** Object:  View [dbo].[BSC_GDA_LineWeight]    Script Date: 12/5/2016 3:50:25 PM ******/
DROP VIEW [dbo].[BSC_GDA_LineWeight]
GO

/****** Object:  View [dbo].[BSC_GDA_LineWeight]    Script Date: 12/5/2016 3:50:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[BSC_GDA_LineWeight]
AS
SELECT   coi.co_num, coi.co_line, coi.item, coi.qty_ordered, it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width AS ItemVolume, it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width * coi.qty_ordered AS LineVolume, it.Uf_BSC_GDA_weight, it.Uf_BSC_GDA_weight * coi.qty_ordered AS LineWeight, 
             it.Uf_BSC_GDA_Height, it.Uf_BSC_GDA_length, it.Uf_BSC_GDA_width
FROM     dbo.coitem_mst AS coi LEFT OUTER JOIN
             dbo.item_mst AS it ON it.item = coi.item

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "coi"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "it"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 404
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_LineWeight'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_LineWeight'
GO

USE [CSI_Demo_App]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_One'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_One'

GO

/****** Object:  View [dbo].[BSC_GDA_Tier_One]    Script Date: 12/5/2016 3:50:36 PM ******/
DROP VIEW [dbo].[BSC_GDA_Tier_One]
GO

/****** Object:  View [dbo].[BSC_GDA_Tier_One]    Script Date: 12/5/2016 3:50:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[BSC_GDA_Tier_One]
AS
SELECT   MRD.MR_RowPointer, SUM(it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_Length * it.Uf_BSC_GDA_Width * coi.qty_ordered_conv) AS line_volume, SUM(it.Uf_BSC_GDA_weight * coi.qty_ordered_conv) AS LineWeight
FROM     dbo.co_mst AS CO LEFT OUTER JOIN
             dbo.coitem_mst AS coi ON CO.co_num = coi.co_num AND CO.site_ref = coi.site_ref LEFT OUTER JOIN
             dbo.item_mst AS it ON it.item = coi.item AND it.site_ref = CO.site_ref LEFT OUTER JOIN
             dbo.BSC_GDA_MilkRunDetail AS MRD ON MRD.Co_Num = CO.co_num
WHERE   (CO.site_ref = 'Dals')
GROUP BY MRD.MR_RowPointer


GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CO"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "coi"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 404
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "it"
            Begin Extent = 
               Top = 405
               Left = 57
               Bottom = 602
               Right = 460
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MRD"
            Begin Extent = 
               Top = 9
               Left = 559
               Bottom = 206
               Right = 804
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_One'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_One'
GO

USE [CSI_Demo_App]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_Zero'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_Zero'

GO

/****** Object:  View [dbo].[BSC_GDA_Tier_Zero]    Script Date: 12/5/2016 3:50:46 PM ******/
DROP VIEW [dbo].[BSC_GDA_Tier_Zero]
GO

/****** Object:  View [dbo].[BSC_GDA_Tier_Zero]    Script Date: 12/5/2016 3:50:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[BSC_GDA_Tier_Zero]
AS
SELECT   CO.co_num, coi.co_line, coi.qty_ordered_conv, coi.item, it.Uf_BSC_GDA_Height, it.Uf_BSC_GDA_length, it.Uf_BSC_GDA_Width, it.Uf_BSC_GDA_Weight, it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width AS ItemVolume, it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width * coi.qty_ordered_conv AS line_volume, 
             it.Uf_BSC_GDA_weight * coi.qty_ordered_conv AS LineWeight
FROM     dbo.co_mst AS CO LEFT OUTER JOIN
             dbo.coitem_mst AS coi ON CO.co_num = coi.co_num AND CO.site_ref = coi.site_ref LEFT OUTER JOIN
             dbo.item_mst AS it ON it.item = coi.item AND it.site_ref = CO.site_ref
WHERE   (CO.site_ref = 'Dals')


GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CO"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "coi"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 404
               Right = 486
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "it"
            Begin Extent = 
               Top = 405
               Left = 57
               Bottom = 602
               Right = 444
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_Zero'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_Tier_Zero'
GO

USE [CSI_Demo_App]
GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPaneCount' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'

GO

EXEC sys.sp_dropextendedproperty @name=N'MS_DiagramPane1' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'

GO

/****** Object:  View [dbo].[BSC_GDA_TruckUsage]    Script Date: 12/5/2016 3:50:58 PM ******/
DROP VIEW [dbo].[BSC_GDA_TruckUsage]
GO

/****** Object:  View [dbo].[BSC_GDA_TruckUsage]    Script Date: 12/5/2016 3:50:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[BSC_GDA_TruckUsage]
AS
SELECT   GTR.TruckModel, GTT.volume AS TruckVolumeCapacity, GTT.weight AS TruckWeightCapacity, SUM(it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width * coi.qty_ordered) AS LineVolume, SUM(it.Uf_BSC_GDA_Height * it.Uf_BSC_GDA_length * it.Uf_BSC_GDA_width * coi.qty_ordered) 
             / GTT.volume AS VolumeUsage, SUM(it.Uf_BSC_GDA_weight * coi.qty_ordered) AS LineWeight, SUM(it.Uf_BSC_GDA_weight * coi.qty_ordered) / GTT.weight AS WeightUsage, co.Uf_BSC_GDA_TruckModel, co.Uf_BSC_GDA_DeliveryDate
FROM     dbo.coitem_mst AS coi LEFT OUTER JOIN
             dbo.item_mst AS it ON it.item = coi.item LEFT OUTER JOIN
             dbo.co_mst AS co ON coi.co_num = co.co_num LEFT OUTER JOIN
             dbo.BSC_GDA_Truck AS GTR ON co.Uf_BSC_GDA_TruckModel = GTR.LicensePlate LEFT OUTER JOIN
             dbo.BSC_GDA_TrackTruck AS GTT ON GTT.model = GTR.TruckModel
GROUP BY co.Uf_BSC_GDA_TruckModel, co.Uf_BSC_GDA_DeliveryDate, GTR.TruckModel, GTT.volume, GTT.weight

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "coi"
            Begin Extent = 
               Top = 9
               Left = 57
               Bottom = 206
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "it"
            Begin Extent = 
               Top = 207
               Left = 57
               Bottom = 404
               Right = 460
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "co"
            Begin Extent = 
               Top = 405
               Left = 57
               Bottom = 602
               Right = 502
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GTR"
            Begin Extent = 
               Top = 9
               Left = 559
               Bottom = 206
               Right = 797
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "GTT"
            Begin Extent = 
               Top = 207
               Left = 517
               Bottom = 404
               Right = 755
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
         Width = 1000
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
       ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'  Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'BSC_GDA_TruckUsage'
GO

