USE [CSI_Demo_App]
GO

/****** Object:  Table [dbo].[BSC_TAK_TT_Admin]    Script Date: 8/5/2016 11:01:40 AM ******/
DROP TABLE [dbo].[BSC_TAK_TT_Admin]
GO

/****** Object:  Table [dbo].[BSC_TAK_TT_Admin]    Script Date: 8/5/2016 11:01:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BSC_TAK_TT_Admin](
	[CreatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_CreatedBy]  DEFAULT (suser_sname()),
	[UpdatedBy] [dbo].[UsernameType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_UpdatedBy]  DEFAULT (suser_sname()),
	[CreateDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_CreateDate]  DEFAULT (getdate()),
	[RecordDate] [dbo].[CurrentDateType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_RecordDate]  DEFAULT (getdate()),
	[RowPointer] [dbo].[RowPointerType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_RowPointer]  DEFAULT (newid()),
	[NoteExistsFlag] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_NoteExistsFlag]  DEFAULT ((0)),
	[InWorkflow] [dbo].[FlagNyType] NOT NULL CONSTRAINT [DF_BSC_TAK_TT_Admin_InWorkflow]  DEFAULT ((0)),
	[CurrentOperation] [nvarchar](80) NULL,
	[EffectiveDate] [datetime] NULL,
	[ObsoleteDate] [datetime] NULL,
	[OperationNumber] [dbo].[OperNumType] NULL,
	[TaktTime] [numeric](18, 0) NULL,
	[WorkCenter] [nvarchar](80) NULL,
 CONSTRAINT [PK_BSC_TAK_TT_Admin] PRIMARY KEY CLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_BSC_TAK_TT_Admin_RowPointer] UNIQUE NONCLUSTERED 
(
	[RowPointer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

